﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<ITargetAdapter> listAdatptor = new List<ITargetAdapter>();
            ContractedEmployee c = new ContractedEmployee();
            c.MonthCount = 20;
            c.MonthlySalary = 200;
       
            PermanentEmployee pe = new PermanentEmployee();
            pe.MonthCount = 10;
            pe.MonthlySalary = 100;
        
            listAdatptor.Add(new AdapterContractedEmployee(c));
            listAdatptor.Add(new AdapterPermanentEmployee(pe));

            foreach (var item in listAdatptor)
            {
              
                item.CalculateAndPrintMonthlySalary();
            
            }
            Console.ReadKey();
        }

    }
    public interface ITargetAdapter
    {
        void CalculateAndPrintMonthlySalary();
     
    }

    public class ContractedEmployee
    {
        public int MonthCount { get; set; }
        public int MonthlySalary { get; set; }

        private int Salary { get; set; }

        public int CalculateContractedEmployeeSalary()
        {
             Salary=this.MonthCount * this.MonthlySalary;
            PrintContractedEmployeeSalary();
            return Salary;
        }
        private void PrintContractedEmployeeSalary() {
            Console.WriteLine("Printed Contracted Employee Salary as:" + Salary);
        }
       
    }

    public class PermanentEmployee
    {
        public int MonthCount { get; set; }
        public int MonthlySalary { get; set; }

        private int Salary { get; set; }
        public int CalculatePermanentEmployeeSalary()
        {
            Salary = this.MonthCount * this.MonthlySalary;
            PrintPermanentEmployeeSalary();
            return Salary;
        }

        private void PrintPermanentEmployeeSalary()
        {
            Console.WriteLine("Printed Permanent Employee Salary as:"+ Salary);
        }

    }



    public class AdapterContractedEmployee : ITargetAdapter
    {
        private ContractedEmployee A { get; set; }

        public AdapterContractedEmployee(ContractedEmployee a)
        {
            this.A = a;
        }

        public void CalculateAndPrintMonthlySalary()
        {
            this.A.CalculateContractedEmployeeSalary();
        }

       
    }

    public class AdapterPermanentEmployee : ITargetAdapter
    {
        private PermanentEmployee B { get; set; }

        public AdapterPermanentEmployee(PermanentEmployee b)
        {
            this.B = b;
        }
        public void CalculateAndPrintMonthlySalary()
        {
            this.B.CalculatePermanentEmployeeSalary();
        }

       
    }





}
