﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            Parallel.Invoke(
                () => PrinStudentDetails()
               , () => PrintEmployeeDetails());

          
            Console.ReadLine();

        }

        private static void PrintEmployeeDetails()
        {
            Singleton employee = Singleton.GetInstance;
            employee.PrintDetails("Employee printed");
        }

        private static void PrinStudentDetails()
        {
            Singleton student = Singleton.GetInstance;
            student.PrintDetails("Student printed");
        }
    }
}
