﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    public  class Singleton
    {

        private static readonly Lazy<Singleton> instance = new Lazy<Singleton>(()=>new Singleton());

        public static Singleton GetInstance
        {
            get
            {
                return instance.Value;
            }
        }


        private static int counter = 0;
        private Singleton()
        {
            counter++;
            Console.WriteLine("Count: " + counter);
        }


        public void PrintDetails(string message)
        {
            Console.WriteLine(message);
        }
    }
}
